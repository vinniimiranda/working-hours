# build environment
FROM node:14-alpine as build
WORKDIR /app
COPY package.json /app/package.json
RUN yarn install --production

COPY package.json /app/package.json
COPY . /app

EXPOSE 3000
CMD ["yarn", "start"]

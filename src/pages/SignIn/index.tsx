import {
  Box, Button, TextField, Typography,
} from '@material-ui/core';
import React from 'react';
import { useHistory } from 'react-router';

const SignIn: React.FC = () => {
  const { push } = useHistory();
  const handleLogin = () => {
    push('/home');
  };
  return (
    <Box height="100vh" width="100%" display="flex" justifyContent="center" alignItems="center">
      <Box display="flex" flexDirection="column">
        <TextField label="E-mail" size="small" variant="outlined" color="secondary" style={{ margin: '.5rem' }} />
        <TextField label="Password" size="small" variant="outlined" color="secondary" style={{ margin: '.5rem' }} />
        <Button variant="contained" onClick={handleLogin} color="secondary" style={{ margin: '.5rem' }}>SignIn</Button>
      </Box>
    </Box>
  );
};

export default SignIn;

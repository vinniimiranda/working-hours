import { Box, Typography } from '@material-ui/core';
import React from 'react';
import styles from './Counters.module.css';

type CountersProps = {
  weekWorkedHours: string;
  monthWorkedHours: string;
}

const Counters: React.FC<CountersProps> = ({ weekWorkedHours, monthWorkedHours }: CountersProps):
  React.ReactElement => (
    <Box flex={1} display="flex" marginBottom="2rem">
      <Box className={styles.workedHoursMonth}>
        <Typography>This Month</Typography>
        <Typography variant="h4" data-testid="month">{monthWorkedHours}</Typography>
      </Box>
      <Box className={styles.workedHoursWeek} id="week">
        <Typography>This Week</Typography>
        <Typography variant="h4" data-testid="week">{weekWorkedHours}</Typography>
      </Box>

    </Box>
);

export default Counters;

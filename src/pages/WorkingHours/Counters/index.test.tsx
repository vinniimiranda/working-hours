import React from 'react';
import { render, screen } from '@testing-library/react';
import Counters from '.';

describe('<Counters />', () => {
  it('should render the counters componet', () => {
    render(<Counters monthWorkedHours="168:00" weekWorkedHours="40:00" />);
    expect(screen.getByTestId('month').textContent).toBe('168:00');
    expect(screen.getByTestId('week').textContent).toBe('40:00');
  });
});

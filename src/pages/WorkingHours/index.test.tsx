import React from 'react';
import {
  cleanup,
  fireEvent,
  render, screen, waitFor,
} from '@testing-library/react';
import axios from 'axios';
import { DateTime } from 'luxon';
import WorkingHours from '.';

describe('WorkingHours page', () => {
  it('Should render the WorkingHours page', () => {
    const { getByTestId, getByRole, getByText } = render(<WorkingHours />);
    expect(getByRole('heading', {
      name: /working hours/i,
    })).toBeInTheDocument();
    expect(getByText(/new entry/i)).toBeInTheDocument();
    expect(getByTestId('isLoading')).toBeInTheDocument();
  });

  it('Should render the WorkingHours with data', async () => {
    const data = [{
      id: 1,
      date: DateTime.utc().set({ weekday: 1 }).toFormat('yyyy-MM-dd'),
      arrival: DateTime.local().set({ weekday: 1, hour: 8, minute: 0 }).toString(),
      lunchStart: DateTime.local().set({ weekday: 1, hour: 12, minute: 0 }).toString(),
      lunchEnd: DateTime.local().set({ weekday: 1, hour: 13, minute: 0 }).toString(),
      exit: DateTime.local().set({ weekday: 1, hour: 17, minute: 5 }).toString(),
    }];

    jest.spyOn(axios, 'get').mockResolvedValue({ data });
    await waitFor(() => render(<WorkingHours />));
    expect(screen.getByTestId('date').textContent).toBeDefined();
    expect(screen.getByTestId('arrival').textContent).toBe('08:00');
    expect(screen.getByTestId('lunchStart').textContent).toBe('12:00');
    expect(screen.getByTestId('lunchEnd').textContent).toBe('13:00');
    expect(screen.getByTestId('exit').textContent).toBe('17:05');
    expect(screen.getByTestId('workedHours').textContent).toBe('08:05');
    expect(screen.getByTestId('month').textContent).toBe('08:05');
    expect(screen.getByTestId('week').textContent).toBe('08:05');
  });
  it('Should render the WorkingHours with error message', async () => {
    jest.spyOn(axios, 'get').mockRejectedValueOnce({ message: 'Network Error' });
    await waitFor(() => render(<WorkingHours />));
    expect(screen.getByTestId('error')).toBeInTheDocument();
    expect(screen.getByRole('heading', {
      name: /an error has occurred: network error/i,
    })).toBeInTheDocument();
  });
  it('Should render the WorkingHours with data and add new entry', async () => {
    const data = [{
      id: 1,
      date: DateTime.utc().set({ weekday: 1 }).toFormat('yyyy-MM-dd'),
      arrival: DateTime.local().set({ weekday: 1, hour: 8, minute: 0 }).toString(),
      lunchStart: DateTime.local().set({ weekday: 1, hour: 12, minute: 0 }).toString(),
      lunchEnd: DateTime.local().set({ weekday: 1, hour: 13, minute: 0 }).toString(),
      exit: DateTime.local().set({ weekday: 1, hour: 17, minute: 5 }).toString(),
    }];
    const newEntry = {
      id: 2,
      date: DateTime.utc().set({ weekday: 2 }).toFormat('yyyy-MM-dd'),
      arrival: DateTime.local().set({ weekday: 2, hour: 9, minute: 19 }).toString(),
      lunchStart: DateTime.local().set({ weekday: 2, hour: 12, minute: 0 }).toString(),
      lunchEnd: DateTime.local().set({ weekday: 2, hour: 13, minute: 0 }).toString(),
      exit: DateTime.local().set({ weekday: 2, hour: 17, minute: 5 }).toString(),

    };

    jest.spyOn(axios, 'get').mockResolvedValueOnce({ data });
    await waitFor(() => render(<WorkingHours />));
    fireEvent.click(screen.getByTestId('addEntry'));
    fireEvent.change(screen.getByLabelText(/time/i), { target: { value: '09:15' } });
    fireEvent.click(screen.getByText(/add entry/i));
    fireEvent.change(screen.getByLabelText(/time/i), { target: { value: '12:00' } });
    fireEvent.click(screen.getByText(/add entry/i));
    fireEvent.change(screen.getByLabelText(/time/i), { target: { value: '13:00' } });
    fireEvent.click(screen.getByText(/add entry/i));
    fireEvent.change(screen.getByLabelText(/time/i), { target: { value: '17:05' } });
    fireEvent.click(screen.getByText(/add entry/i));

    jest.spyOn(axios, 'post').mockResolvedValueOnce({ data: newEntry });
    jest.spyOn(axios, 'get').mockResolvedValueOnce({ data: [...data, newEntry] });
    cleanup();
    await waitFor(() => render(<WorkingHours />));
    expect(screen.getByTestId('month').textContent).toBe('14:51');
    expect(screen.getByTestId('week').textContent).toBe('14:51');
  });
});

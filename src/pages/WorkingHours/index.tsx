/* eslint-disable no-shadow */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import axios, { AxiosError } from 'axios';
import { DateTime, Duration } from 'luxon';
import {
  Box,
  Button,
  CircularProgress,
  Typography,
} from '@material-ui/core';

import WorkingHoursType from '../../types/working-hours.type';
import DataTable from './DataTable';
import NewEntryModal from './NewEntryModal';
import Counters from './Counters';

enum EntryTypes {
  ARRIVAL = 'ARRIVAL',
  LAUNCH_START = 'LAUNCH_START',
  LAUNCH_END = 'LAUNCH_END',
  EXIT = 'EXIT',

}

const WorkingHours: React.FC = () => {
  const [workedHours, setWorkedHours] = useState<WorkingHoursType[]>([]);
  const [entryType, setEntryType] = useState<EntryTypes>(EntryTypes.ARRIVAL);
  const [openModal, setOpenModal] = useState(false);
  const [newEntry, setNewEntry] = useState<Partial<WorkingHoursType>>();
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<AxiosError>();

  useEffect(() => {
    if (entryType === EntryTypes.ARRIVAL) {
      axios.get('http://localhost:3333/workingHours').then(({ data }) => {
        setWorkedHours(
          data.map((day: any) => ({
            id: day.id,
            date: DateTime.fromFormat(day.date, 'yyyy-MM-dd', { zone: 'UTC' }),
            arrival: DateTime.fromISO(day.arrival),
            lunchStart: DateTime.fromISO(day.lunchStart),
            lunchEnd: DateTime.fromISO(day.lunchEnd),
            exit: DateTime.fromISO(day.exit),
          })).sort((a: any, b: any) => (a.date > b.date ? -1 : 1)),
        );
      }).catch((e) => {
        setError(e);
      }).finally(() => setIsLoading(false));
    }
  }, [entryType]);

  const handleOpenModal = () => {
    setOpenModal(true);
  };
  const handleClosenModal = () => {
    setOpenModal(false);
  };

  const handleAddEntry = (time: string) => {
    const [hour, minutes] = time.split(':');
    const pickedTime = DateTime.utc().set({ hour: Number(hour), minute: Number(minutes) });
    switch (entryType) {
      case EntryTypes.ARRIVAL: {
        setNewEntry({
          arrival: pickedTime,
        });
        setEntryType(EntryTypes.LAUNCH_START);
        return true;
      }
      case EntryTypes.LAUNCH_START: {
        if (newEntry?.arrival && newEntry?.arrival >= pickedTime) {
          return alert('Invalid Time');
        }
        setNewEntry({
          ...newEntry,
          lunchStart: pickedTime,
        });
        setEntryType(EntryTypes.LAUNCH_END);
        return true;
      }
      case EntryTypes.LAUNCH_END: {
        if (newEntry?.lunchStart && newEntry?.lunchStart >= pickedTime) {
          return alert('Invalid Time');
        }
        setNewEntry({
          ...newEntry,
          lunchEnd: pickedTime,
        });
        setEntryType(EntryTypes.EXIT);
        return true;
      }
      case EntryTypes.EXIT: {
        if (newEntry?.lunchEnd && newEntry?.lunchEnd >= pickedTime) {
          return alert('Invalid Time');
        }
        return axios.post('http://localhost:3333/workingHours', {
          ...newEntry,
          date: DateTime.local().toFormat('yyyy-MM-dd'),
          exit: pickedTime,
        }).then(() => {
          setEntryType(EntryTypes.ARRIVAL);
          handleClosenModal();
          return true;
        });
      }
      default: {
        return false;
      }
    }
  };

  const calculateWorkedHoursPerDay = (day: WorkingHoursType): Duration => {
    const luchTime = day?.lunchEnd?.diff(day?.lunchStart, 'hours');
    const workedHours = day?.exit?.diff(day?.arrival, 'hours');
    return workedHours?.minus({ hours: luchTime?.hours });
  };

  const weekTotalHours = () => {
    const firstDayOfTheWeek = DateTime.utc().set({
      weekday: 1, hour: 0, minute: 0, second: 0, millisecond: 0,
    });
    const lastDayOfTheWeek = DateTime.utc().set({
      weekday: 5, hour: 0, minute: 0, second: 0, millisecond: 0,
    });

    const totals = workedHours
      .filter((day) => day.date >= firstDayOfTheWeek && day.date <= lastDayOfTheWeek)
      .map((day) => calculateWorkedHoursPerDay(day));

    if (totals.length) {
      const result = totals.reduce((a, b) => a.plus(b));
      if (result) return result.toFormat('hh:mm');
    }
    return '00:00';
  };

  const monthTotalHours = () => {
    const firstDayOfTheMonth = DateTime.utc().set({
      day: 1, hour: 0, minute: 0, second: 0, millisecond: 0,
    });
    const lastDayOfTheMonth = DateTime.utc().set({
      day: DateTime.utc().daysInMonth, hour: 23, minute: 59, second: 59, millisecond: 999,
    });
    const totals = workedHours
      .filter((day) => day.date >= firstDayOfTheMonth && day.date < lastDayOfTheMonth)
      .map((day) => calculateWorkedHoursPerDay(day));
    if (totals.length) {
      const result = totals.reduce((a, b) => a.plus(b));
      if (result) return result.toFormat('hh:mm');
    }
    return '00:00';
  };

  return (
    <Box display="flex" flexDirection="column">
      <Typography variant="h3">
        Working Hours
      </Typography>
      <Box flex={1} display="flex" justifyContent="flex-end" marginBottom="2rem">
        <Button
          variant="contained"
          color="secondary"
          onClick={handleOpenModal}
          data-testid="addEntry"
        >
          New Entry
        </Button>
      </Box>
      <Counters weekWorkedHours={weekTotalHours()} monthWorkedHours={monthTotalHours()} />
      {isLoading && (
        <Box data-testid="isLoading" display="flex" height="40vh" alignItems="center" justifyContent="center">
          <CircularProgress size={60} />
        </Box>
      )}
      {error && (
        <Box data-testid="error">
          <Typography variant="h4">
            {`An error has occurred: ${error.message}`}
          </Typography>
        </Box>
      )}

      {(!isLoading && !error) && (
        <DataTable
          data-testid="data-table"
          entries={workedHours}
          calculateWorkingHours={calculateWorkedHoursPerDay}
        />
      )}
      <NewEntryModal
        open={openModal}
        handleClose={handleClosenModal}
        newEntry={newEntry}
        handleAddEntry={handleAddEntry}

      />
    </Box>
  );
};
export default WorkingHours;

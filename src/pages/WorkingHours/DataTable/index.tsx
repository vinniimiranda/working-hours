import React from 'react';
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import { Duration } from 'luxon';
import WorkingHours from '../../../types/working-hours.type';
import styles from './DataTable.module.css';

type DataTableProps ={
  entries: WorkingHours[],
  calculateWorkingHours: (day: WorkingHours) => Duration
}

const DataTable: React.FC<DataTableProps> = ({ entries, calculateWorkingHours }: DataTableProps):
React.ReactElement => (
  <TableContainer component={Paper} className={styles.container}>
    <Table stickyHeader aria-label="simple table">
      <TableHead title="Entries">
        <TableRow>
          <TableCell>Date</TableCell>
          <TableCell>Arrival</TableCell>
          <TableCell>Lunch Start</TableCell>
          <TableCell>Lunch End</TableCell>
          <TableCell>Exit</TableCell>
          <TableCell>Worked Hours</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {entries?.map((entry) => (
          <TableRow key={entry.id}>
            <TableCell data-testid="date">{entry.date.toFormat('DDDD')}</TableCell>
            <TableCell data-testid="arrival">{entry.arrival.toFormat('HH:mm')}</TableCell>
            <TableCell data-testid="lunchStart">{entry.lunchStart.toFormat('HH:mm')}</TableCell>
            <TableCell data-testid="lunchEnd">{entry.lunchEnd.toFormat('HH:mm')}</TableCell>
            <TableCell data-testid="exit">{entry.exit.toFormat('HH:mm')}</TableCell>
            <TableCell>
              <Typography data-testid="workedHours" className={styles.workedHoursLabel}>
                {calculateWorkingHours(entry).toFormat('hh:mm')}
              </Typography>
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  </TableContainer>
);

export default DataTable;

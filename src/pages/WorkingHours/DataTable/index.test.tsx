import React from 'react';
import { render, screen } from '@testing-library/react';
import { DateTime, Duration } from 'luxon';
import DataTable from '.';

describe('<DataTable />', () => {
  it('should render the datatable componet', () => {
    const entries = [
      {
        id: 1,
        date: DateTime.utc(),
        arrival: DateTime.utc().set({ hour: 8, minute: 0 }),
        lunchStart: DateTime.utc().set({ hour: 12, minute: 0 }),
        lunchEnd: DateTime.utc().set({ hour: 13, minute: 0 }),
        exit: DateTime.utc().set({ hour: 17, minute: 0 }),
      },
    ];
    const calculateWorkedHoursPerDay = (day: any): Duration => {
      const luchTime = day?.lunchEnd?.diff(day?.lunchStart, 'hours');
      const workedHours = day?.exit?.diff(day?.arrival, 'hours');
      return workedHours?.minus({ hours: luchTime?.hours });
    };
    render(<DataTable entries={entries} calculateWorkingHours={calculateWorkedHoursPerDay} />);
    expect(screen.getByTestId('date').textContent).toBeDefined();
    expect(screen.getByTestId('arrival').textContent).toBe('08:00');
    expect(screen.getByTestId('lunchStart').textContent).toBe('12:00');
    expect(screen.getByTestId('lunchEnd').textContent).toBe('13:00');
    expect(screen.getByTestId('exit').textContent).toBe('17:00');
    expect(screen.getByTestId('workedHours').textContent).toBe('08:00');
  });
});

import React, { useState } from 'react';
import {
  Box,
  Button,
  Dialog,
  Grid,
  TextField,
  Typography,
  useMediaQuery,
  useTheme,
} from '@material-ui/core';
import { DateTime } from 'luxon';

type NewEntryModalProps = {
  open: boolean;
  handleClose: (state: boolean) => void;
  newEntry: any
  handleAddEntry: (time: string) => void;
}

const NewEntryModal: React.FC<NewEntryModalProps> = ({
  open, handleClose, newEntry, handleAddEntry,
}: NewEntryModalProps): React.ReactElement => {
  const [time, setTime] = useState(DateTime.local().toFormat('HH:mm'));
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('sm'));
  return (
    <Dialog open={open} onClose={handleClose}>
      <Box padding={matches ? '2rem' : '1rem'} minWidth={matches ? '30rem' : '100%'}>
        <Typography
          variant="h5"
          style={{
            marginBottom: 20,
          }}
        >
          New Entry
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={12} md={6}>
            <Box display="flex" flexDirection="column">
              <TextField
                id="time"
                label="Time"
                type="time"
                value={time}
                onChange={({ target }) => setTime(target.value)}
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{
                  step: 300,
                }}
              />
              <Button
                variant="contained"
                color="secondary"
                style={{ marginTop: '1rem' }}
                onClick={() => handleAddEntry(time)}
              >
                Add Entry
              </Button>
            </Box>
          </Grid>
          <Grid item xs={12} sm={12} md={6}>
            <Box
              display="flex"
              flexDirection="column"
              justifyContent="center"
              alignItems="center"
            >
              <Typography>Today Entry History</Typography>
              {newEntry?.arrival && (
                <Typography>
                  {`Arrival: ${newEntry.arrival.toFormat('hh:mm')}`}
                </Typography>
              )}
              {newEntry?.lunchStart && (
                <Typography>
                  {`Launch Start: ${newEntry.lunchStart.toFormat('hh:mm')}`}
                </Typography>
              )}
              {newEntry?.lunchEnd && (
                <Typography>
                  {`Launch End: ${newEntry.lunchEnd.toFormat('hh:mm')}`}
                </Typography>
              )}

            </Box>
          </Grid>
        </Grid>
      </Box>
    </Dialog>
  );
};

export default NewEntryModal;

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import NewEntryModal from '.';

describe('<NewEntryModal  />', () => {
  it('should render the counters componet', () => {
    const newEntry: any = {};
    const handleAddEntry = (time: string) => {
      newEntry.arrival = time;
    };
    const handleClose = jest.fn;
    render(<NewEntryModal
      open
      handleAddEntry={handleAddEntry}
      handleClose={handleClose}
      newEntry={newEntry}
    />);
    expect(screen.getByRole('heading', {
      name: /new entry/i,
    })).toBeInTheDocument();
    expect(screen.getByLabelText(/time/i)).toBeInTheDocument();
    expect(screen.getByText(/add entry/i)).toBeInTheDocument();
  });
  it('should add arrival to new entry', () => {
    const newEntry: any = {};
    const handleClose = jest.fn();

    const handleAddEntry = (time: string) => {
      newEntry.arrival = time;
      handleClose();
    };

    render(<NewEntryModal
      open
      handleAddEntry={handleAddEntry}
      handleClose={handleClose}
      newEntry={newEntry}
    />);
    fireEvent.change(screen.getByLabelText(/time/i), { target: { value: '08:00' } });
    fireEvent.click(screen.getByText(/add entry/i));
    expect(newEntry).toHaveProperty('arrival');
    expect(newEntry.arrival).toBe('08:00');
    expect(handleClose).toBeCalled();
  });
});

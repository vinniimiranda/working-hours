import React from 'react';
import { Box, useMediaQuery, useTheme } from '@material-ui/core';
import './App.css';
import ThemeContext from './context/ThemeContext';
import Routes from './routes';

function App() {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up('sm'));
  return (
    <ThemeContext>
      <Box padding={matches ? '3rem' : '1rem'}>
        <Routes />
      </Box>
    </ThemeContext>
  );
}

export default App;

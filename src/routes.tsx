import React from 'react';
import { Switch } from 'react-router';
import { Route, BrowserRouter } from 'react-router-dom';
import WorkingHours from './pages/WorkingHours';
import SignIn from './pages/SignIn';

export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={SignIn} />
        <Route path="/home" exact component={WorkingHours} />
      </Switch>
    </BrowserRouter>
  );
}

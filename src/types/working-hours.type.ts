import { DateTime } from 'luxon';

type WorkingHours = {
  id: number,
  date: DateTime;
  arrival: DateTime;
  lunchStart: DateTime;
  lunchEnd: DateTime;
  exit: DateTime;
}

export default WorkingHours;

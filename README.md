# TIMESHEET

### SUMARY
This is a React project generated with create-react-app, i am using Typescript and Material UI to be more productive.

The focus of this project it's offer an App to Working Hours Management.

### Running the Project
You can easily run this project with docker running the following command:
`docker-compose up`
This will take some time, once is finished you can find the React App running at: http://localhost:3000


### Unit Test
This project contains some unit tests, just type on terminal `yarn test` to run all of them.

### App Routes
There is only two routes:
* http://localhost:3000/ - SignIn page with email and password input
* http://localhost:3000/home - Working Hours page to manage the working hours for each employee

